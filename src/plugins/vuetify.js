import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#D3C3BC',
        secondary: '#A4A4A4',
        accent: '#D8EBF1',
        // info: '#219cc9',
        info: '#C00000',
      },
    },
  },
})
