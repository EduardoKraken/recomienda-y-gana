import Vue from 'vue'
import './plugins/base'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueResource from "vue-resource";


Vue.config.productionTip = false

Vue.use(VueResource);

// Vue.http.options.root = 'http://localhost:3004/';
Vue.http.options.root = 'https://escuelakpi.club/kpi/';


new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
