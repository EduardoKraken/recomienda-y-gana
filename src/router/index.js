import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/:escuela/:id_alumno/:nombre_alumno/:matricula', name: 'home', component: HomeView },
]

const router = new VueRouter({
  routes
})

export default router
