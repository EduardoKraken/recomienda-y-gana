import Vue from 'vue'
import BaseHeading from '@/components/base/Heading'
import BaseSubheading from '@/components/base/Subheading'
import BaseText from '@/components/base/Text'
import BaseBubble1 from '@/components/base/Bubble1'
import BaseBubble2 from '@/components/base/Bubble2'

Vue.component('BaseHeading', BaseHeading)
Vue.component('BaseSubheading', BaseSubheading)
Vue.component('BaseText', BaseText)
Vue.component('BaseBubble1', BaseBubble1)
Vue.component('BaseBubble2', BaseBubble2)
